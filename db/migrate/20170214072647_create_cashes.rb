class CreateCashes < ActiveRecord::Migration[5.0]
  def change
    create_table :cashes do |t|
      t.float :value
      t.integer :category_id
      t.integer :expense_id

      t.timestamps
    end
  end
end
