class AddUserReferencesToOtherModel < ActiveRecord::Migration[5.0]
  def change
    add_column :incomes, :user_id, :integer
    add_column :categories, :user_id, :integer
    add_column :expenses, :user_id, :integer
  end
end
