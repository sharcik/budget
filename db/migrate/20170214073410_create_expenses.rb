class CreateExpenses < ActiveRecord::Migration[5.0]
  def change
    create_table :expenses do |t|
      t.float :value
      t.references :category, foreign_key: true
      t.references :cash, foreign_key: true

      t.timestamps
    end
  end
end
