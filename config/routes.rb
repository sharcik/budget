Rails.application.routes.draw do

  get 'expenses/index'

  root 'pages#main'

  devise_for :users

  match '/incomes', to: 'incomes#index', via: 'get'
  match '/incomes', to: 'incomes#create', via: 'post'

  resources :categories

  resources :cash, except: [:create]
  match '/cashes', to: 'cash#create', via: 'post'

  resources :expenses

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
