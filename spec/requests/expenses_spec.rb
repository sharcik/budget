require 'spec_helper'
require 'rails_helper'

RSpec.describe "Expenses", type: :request do

  before(:each) do
    user = User.create!(email: "Sharcik@mail.ru", password: "12345678", password_confirmation: "12345678")
    visit new_user_session_path
    fill_in :email, with: user.email
    fill_in :password, with: user.password
    click_button "Sign in"
    visit expenses_path
  end

  subject {page}

  describe "index page " do
    it { should have_content("Select Category") }
    it { should_not has_css?("pagination") }

    describe "maximum 6 expenses on page" do
      before do
        8.times do |i|
          Expense.create(value: i*100, date: 2017-01-16,cash_id: i+1, category_id: i+1)
        end
        visit expenses_path
      end

      it { should has_css?("expense_row", count: 6) }

      context "check pagination" do
        it { should has_css?("pagination") }
      end
    end
  end
end
