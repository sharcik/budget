require 'spec_helper'
require 'rails_helper'

RSpec.describe "Incomes", type: :request do

  before(:each) do
    @user = User.create!(email: "Sharcik@mail.ru", password: "12345678", password_confirmation: "12345678")
    visit new_user_session_path
    fill_in :email, with: @user.email
    fill_in :password, with: @user.password
    click_button "Sign in"
    visit incomes_path
  end

  subject {page}

  it { should_not has_css?("pagination") }

  describe "maximum 6 incomes on page" do
    before do
      8.times do |i|
        Income.create(title: "sddvsdv",value: i*100,date: 2017-01-16, user_id: @user.id)
      end
      visit incomes_path
    end

    it { should has_css?("income_row", count: 6) }

    context "check pagination" do
      it { should has_css?("pagination") }
    end
  end

  describe "check incomes content " do
    it { should have_content("Create income") }
  end

  describe "create incomes" do
    let(:submit) {"Create income"}

    context "check count incomes when fail" do
      it do 
        expect {click_button submit}.to_not change(Income, :count) 
      end
    end

    context "check error when fail" do
      before { click_button submit}

      it { should have_selector('div.alert-danger') }
    end

    context "success" do

      before do
        fill_in 'income[value]', with: 100.0
        fill_in 'income[title]', with: 'dsfdsfds'
        select 'March', from: 'income[date(2i)]'
        select 2016, from: 'income[date(1i)]'
        select 10, from: 'income[date(3i)]'
      end

      it "check count incomes when success", js: true do
        expect{click_button submit; sleep 1}.to change{Income.count}
      end
    end
  end
end
