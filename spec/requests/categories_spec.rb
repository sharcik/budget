require 'rails_helper'

RSpec.describe "Categories", type: :request do

  before(:each) do
    @user = User.create(email: "Sharcik@mail.ru", password: "12345678", password_confirmation: "12345678")
    visit new_user_session_path
    fill_in :email, with: @user.email
    fill_in :password, with: @user.password
    click_button
    visit categories_path
  end

  subject {page}
  
  it { should_not has_css?("pagination") }

  describe "maximum 6 categories on page" do
    before do
      8.times do |i|
        Category.create(title: "sddvsdv", user_id: @user.id)
      end
      visit categories_path
    end

    it { should has_css?("category_row", count: 6) }

    context "check pagination" do
      it { should has_css?("pagination") }
    end
  end

  describe "create category" do
    let(:submit) {"Create Category"}

    context "check count categories when fail" do
      it { expect {click_button submit}.to_not change(Category, :count) }
    end

    context "check error when fail" do
      before { click_button submit}

      it { should have_selector('div.alert-danger') }
    end

    context "success" do

      before do
        fill_in "category[title]", with: 'ghngh'
      end

      it "check categories count + 1", js:true do
        expect {click_button submit;sleep 1}.to change(Category, :count)
      end
    end
  end

  describe "Show page" do 
    let(:rename_submit) {"Rename category"}
    let(:create_cash) {"Set cash"}
    let(:create_expense) {"Save expense"}


    before(:each) do
      @category = @user.categories.create(title: "First")
      visit category_path(@category.id)
    end 

    describe "check categories content " do
      it { should have_content(@category.title) }
    end

    describe "display none expenses form without cash" do
      it { should_not have_content('Save expense') }
    end

    describe "display none cash form if cash exist" do
      it { should_not have_content('Set cash') }
    end

    describe "rename category", js: true do
      before { find('#rename_form').click }

      describe "fail" do
        it { expect {click_button rename_submit}.to_not change(@category, :title) }
      end

      describe "success", js: true do
        before do 
          @category.title = 'First'
          fill_in "category[title]", with: 'Second'
          click_button rename_submit
          sleep 1
        end
        it { expect(Category.find(@category.id).title).to eq "Second" }
        it { should have_selector('div.alert-success') }
      end
    end

    describe "create cash" do

      describe "success" do
        before { fill_in "cash[value]", with: '1000';sleep 1 }

        it "check count cash when success" do
          expect{click_button create_cash; sleep 1}.to change(Cash,:count)
          should have_selector('div.alert-success')
        end
      end
    end

    describe "create expenses" do
      describe "fail" do
        before do
          @cash = Cash.create(value: '30', category_id: @category.id)
          visit category_path(@category.id)
        end

        it { expect(Expense.count).to eq 0 }
      end

      describe "success" do
        before(:each) do
          @cash = Cash.create(value: '30', category_id: @category.id)
          visit category_path(@category.id)
          fill_in 'expense[value]', with: 'dsfdsfds'
          select 'March', from: 'expense[date(2i)]'
          select 2016, from: 'expense[date(1i)]'
          select 10, from: 'expense[date(3i)]'
        end

        it { expect{click_button create_expense; sleep 1}.to change(Expense,:count) }
      end      
    end
  end
end
