require 'rails_helper'

RSpec.describe Category, type: :model do

  before { @category = Category.new(title: "salary", user_id: 1) }

  subject { @category }

  it { should respond_to(:title) }
  it { should respond_to(:user_id) }

  describe "check validation presense user_id" do
    before do   
      @category.user_id = ""
    end
    it { should validate_presence_of(:user_id) }
  end

  describe "check validation presense title" do
    before do   
      @category.title = ""
    end
    it { should validate_presence_of(:title) }
  end

  describe "check validation length" do
    before { @category.title = "T" * 51}
    it { should validate_length_of(:title).is_at_most(50) }
  end

  describe "Associations" do  
    it { should belong_to(:user) }
    it { should have_many(:expenses) }
    it { should have_one(:cash) }
  end


  describe "after deleting category should delete" do
    context "cash" do
      before do 
        @cash = Cash.new(value: 100.0, category_id: @category.id)
        @category.destroy
      end

      subject { @cash }

      it { not exist }
    end

    context "expense" do
      before do 
        @expense = @category.expenses.new(value: 1000.0)
        @category.destroy
      end

      subject { @expense }

      it { not exist }
    end
  end
end
