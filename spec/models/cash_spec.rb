require 'rails_helper'

RSpec.describe Cash, type: :model do
  
  before do
    @category = Category.new(title: 'dsdscds')
    @cash = Cash.new(value: 1000, category_id: @category.id) 
  end

  subject { @cash }

  it { should respond_to(:value) }

  describe "category can't be empty" do
    it { should validate_presence_of(:category_id) }
  end

  describe "Associations" do
    it { should have_many(:expenses) }
    it { should belong_to(:category) }
  end
end
