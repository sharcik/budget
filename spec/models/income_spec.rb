require 'rails_helper'


RSpec.describe Income, type: :model do
  
  before { @income = Income.new(title: "salary", value: 1000, date: 2017-01-16) }

  subject { @income }

  it { should respond_to(:title) }
  it { should respond_to(:value) }
  it { should respond_to(:date) }


  describe "check validation presense title" do
    before do   
      @income.title = ""
    end
    it { should validate_presence_of(:title) }
  end

  describe "check validation presense value" do
    before do   
      @income.value = ""
    end
    it { should validate_presence_of(:value) }
  end

  describe "check validation presense date" do
    before do   
      @income.date = ""
    end
    it { should validate_presence_of(:date) }
  end

  describe "check validation length" do
    before { @income.title = "T" * 51}
    it { should validate_length_of(:title).is_at_most(50) }
  end

  describe "Associations" do  
    it { should belong_to(:user) }
  end
end
