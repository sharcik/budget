require 'rails_helper'

RSpec.describe Expense, type: :model do
  
  before do
    @category = Category.new(title: 'dsdscds')
    @cash     = Cash.new(value: 1000.0, category_id: @category.id)
    @expense  = @category.expenses.new(value: 300.0, cash_id: @cash.id)
  end

  subject { @expense }

  it { should respond_to(:value) }
  it { should respond_to(:category_id) }
  it { should respond_to(:cash_id) }

  describe "value can't be empty" do
    before { @expense.value = '' }
    it { should validate_presence_of(:value) }
  end

  describe "Associations" do
    it { should belong_to(:cash) }
    it { should belong_to(:category) }
  end
end
