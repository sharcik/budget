class Category < ApplicationRecord
  validates :title,   presence: true, length: { maximum: 50 }
  validates :user_id, presence: true

  belongs_to :user

  has_one :cash,      dependent: :destroy
  has_many :expenses, dependent: :destroy
end
