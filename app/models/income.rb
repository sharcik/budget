class Income < ApplicationRecord
  validates :title, presence: true, length: { maximum: 50 }
  validates :value, presence: true
  validates :date,  presence: true

  belongs_to :user

  scope :sort_with_date, -> (direction){ order(date: direction) }
end
