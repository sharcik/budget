class Cash < ApplicationRecord
  validates :value,       presence: true, length: { maximum: 50 }
  validates :category_id, presence: true

  belongs_to :category
  has_many   :expenses
end
