class PagesController < ApplicationController
  def main
    @categories = current_user.categories.page(params[:page]).per(6) 
  end
end
