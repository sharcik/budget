class ExpensesController < ApplicationController
  def index
    @expenses   = current_user.expenses
    @expenses   = @expenses.where(category_id: params[:category_id]) if params[:category_id].present?
    @expenses   = @expenses.page(params[:page]).per(6)
    @categories = current_user.categories
  end


  def create
    expense  = current_user.expenses.new(expense_params)
    cash = Cash.find(expense.cash_id)
    category = Category.find(expense.category_id)

    if expense.value <= cash.value
      if expense.save 
        cash.value = cash.value - expense.value
        if cash.value == 0
          cash.save
          flash[:success] = "You can buy #{expense.category.title}!"
          redirect_to category
        else cash.value > 0
          cash.save 
          flash[:success] = "You still have to buy #{expense.cash.value} $!"
          redirect_to category
        end
      else 
        flash[:danger] = "Fill expense field, please!"
        redirect_to category
      end
    else
      flash[:danger] = "Expense value more then cash!"
      redirect_to category
    end

  end

  private

  def expense_params
    params.require(:expense).permit(:value, :category_id, :cash_id, :date)
  end 

end
