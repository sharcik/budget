class IncomesController < ApplicationController
  def index
    @direction ||= "DESC"
    @direction = params[:date] if params[:date].present? and params[:date] != ""


    @incomes = current_user.incomes.order(date: @direction).page(params[:page]).per(6)
    @income  = Income.new

  end

  def create

    income = current_user.incomes.new(income_params)

    if income.save
      flash.now[:success] = "Category was created"
      @incomes = current_user.incomes.page(params[:page]).per(6)
    else
      flash[:danger] = "Fill all fields, please!"
      redirect_to incomes_path
    end

  end


  private

  def income_params
    params.require(:income).permit(:title, :value, :date)
  end 

end
