class CategoriesController < ApplicationController

  def index
    @categories = current_user.categories.page(params[:page]).per(6)
    @category   = current_user.categories.new
  end

  def show
    @category = Category.find(params[:id])

    if @category.cash
      @cash = @category.cash
    else
      @cash = Cash.new
    end

    @expense  = Expense.new
    @expenses = @category.expenses.order(date: 'DESC').page(params[:page]).per(6)
  end

  def update
    @category = Category.find(params[:id])

    if @category.update_attributes(category_params)
      flash[:success] = "Category was renamed"
      redirect_to @category
    else
      flash[:danger] = "Category not updated"
      redirect_to @category
    end
  end


  def create
    category = current_user.categories.new(category_params)

    if category.save
      flash.now[:success] = "Category was created"
      @categories = current_user.categories.page(params[:page]).per(6)
    else
      flash[:danger] = "Fill category field, please!"
      redirect_to categories_path
    end

  end

  def destroy
    category = Category.find(params[:id])

    if category.destroy
      @categories = current_user.categories.page(params[:page]).per(6)
    else
      flash.now[:danger] = "Category is not removed!"
      redirect_to categories_path
    end

  end

  private

  def category_params
    params.require(:category).permit(:title)
  end 

end
