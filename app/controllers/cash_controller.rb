class CashController < ApplicationController

  def create
    cash = Cash.new(cash_params)

    if cash.save
      flash[:success] = "You planned #{cash.value.to_s} $ for this category"
      redirect_to cash.category
    else
      flash.now[:danger] = "Fill sash field, please!"
      redirect_to Category.find(params[:category_id])
    end

  end

  def update
    cash = Cash.find(params[:id])

    if cash.update_attributes(cash_params)
      flash[:success] = "You planned #{cash.value.to_s} $ for this category"
      redirect_to cash.category
    else
      flash.now[:danger] = "Fill cash field, please!"
      redirect_to Category.find(params[:category_id])
    end

  end

  private

  def cash_params
    params.require(:cash).permit(:value, :category_id)
  end 

end
