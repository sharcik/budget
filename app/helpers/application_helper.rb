module ApplicationHelper

  def all_cash_incomes
    incomes  = current_user.incomes.inject(0) {|sum,elem| sum += elem.value } if current_user.incomes
    expenses = current_user.expenses.inject(0) {|sum,elem| sum += elem.value } if current_user.expenses
    incomes - expenses
  end

  def all_cash
    result = 0
    if current_user.categories
      current_user.categories.each do |elem| 
        if elem.cash
          result += elem.cash.value 
        end
      end
    end
    result
  end

  def check_balance
    if all_cash_incomes > all_cash
      (all_cash_incomes - all_cash).to_s + ' $' 
    else
      'No money for planning!'
    end
  end

  def calculate_expenses(category)
    category.expenses.inject(0) {|sum,elem| sum += elem.value}
  end

end
